import { Time } from '@angular/common';

export class Slot {
  constructor(
    public startTime: string,
    public endTime: string) { }
}

export class Booking {
  public _id: string;
  constructor(
    public id: string,
    public date: string,
    public slot: Slot,
    public capacity: number,
    public needsTelephone: boolean,
    public needsProjector: boolean
  ) { }
}
