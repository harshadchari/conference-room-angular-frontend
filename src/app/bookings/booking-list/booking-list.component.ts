import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CompanyService } from 'src/app/companies/company.service';
import { Booking } from '../booking.model';

@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css']
})
export class BookingListComponent implements OnInit {
  constructor(public route: ActivatedRoute, public companyService: CompanyService) { }

  bookings: Booking[] = [];
  public roomId: string;
  isLoading = false;

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('roomId')) {
        this.roomId = paramMap.get('roomId');

        this.isLoading = true;
        this.companyService.getRoomBookings(this.roomId).subscribe(bookingData => {
          this.isLoading = false;
          console.log(bookingData);
          this.bookings = bookingData.booking;
        });
      } else {
        // this.mode = 'create';
      }
    });
  }

}
