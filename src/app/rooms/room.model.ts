export interface Room {
  id: string;
  name: string;
  maxCapacity: number;
  hasProjector: Boolean;
  hasTelephone: Boolean;
}
